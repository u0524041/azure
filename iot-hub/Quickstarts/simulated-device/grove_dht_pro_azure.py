import grovepi
import math
import random
import time
import threading 

from azure.iot.device import IoTHubDeviceClient, Message
# Connect the Grove Temperature & Humidity Sensor Pro to digital port D4
# This example uses the blue colored sensor.
# SIG,NC,VCC,GND
sensor = 4  # The Sensor goes on digital port 4.
CONNECTION_STRING = "HostName=0524041-iothub.azure-devices.net;DeviceId=0524041-div;SharedAccessKey=Nd2fl2kZ0gFDGjfGcxZyo/gS0rS5HM7R6CFtKTrGnR4="
# temp_humidity_sensor_type
# Grove Base Kit comes with the blue sensor.
blue = 0    # The Blue colored sensor.
white = 1   # The White colored sensor.

RECEIVED_MESSAGES = 0
TEMPERATURE = 20.0 # init The Temperature Data
HUMIDITY = 60      # init The Humidity Data
MSG_TXT = '{{"temperature": {temperature},"humidity": {humidity}}}'
STATUS = 1

def iothub_client_init():
    # Create an IoT Hub client
    client = IoTHubDeviceClient.create_from_connection_string(CONNECTION_STRING)
    return client

def message_listener(client):
    global RECEIVED_MESSAGES
    while True:
        global STATUS
        message = client.receive_message()

        if vars(message)['data'] == "1" :
            STATUS = 1
            print ( "Sensor Status : ON" )
        elif vars(message)['data'] == "0" :
            STATUS = 0
            print ( "Sensor Status : OFF" )
            
        RECEIVED_MESSAGES += 1
        print("\nMessage received:")

        #print data and both system and application (custom) properties
        print('-------------')
        for property in vars(message).items():
            print ("    {0}".format(property))
        print('-------------')

        print( "Total calls received: {}\n".format(RECEIVED_MESSAGES))

def iothub_client_telemetry_sample_run():

    try:
        client = iothub_client_init()
        print ( "IoT Hub device sending periodic messages, press Ctrl-C to exit" )

        message_listener_thread = threading.Thread(target=message_listener, args=(client,))
        message_listener_thread.daemon = True
        message_listener_thread.start()

        while 1:
            # Build the message with simulated telemetry values.
            #temperature = TEMPERATURE + (random.random() * 15)
            #humidity = HUMIDITY + (random.random() * 20)
            while STATUS:
                try:
                	[temperature,humidity] = grovepi.dht(sensor,blue)
                	if math.isnan(temperature) == False and math.isnan(humidity) == False:
                		msg_txt_formatted = MSG_TXT.format(temperature=temperature, humidity=humidity)
                except IOError:
                	print ("Error")

                message = Message(msg_txt_formatted)
                # Add a custom application property to the message.
                # An IoT hub can filter on these properties without access to the message body.

                # Send the message.
                print( "Sending message: {}".format(message) )
                client.send_message(message)
                print ( "Message successfully sent" )
                time.sleep(1)

    except KeyboardInterrupt:
        print ( "IoTHubClient sample stopped" )

if __name__ == '__main__':
    print ( "IoT Hub Quickstart #1 - Simulated device" )
    print ( "Press Ctrl-C to exit" )
    iothub_client_telemetry_sample_run()
